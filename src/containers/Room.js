/* eslint-disable no-nested-ternary */
import React from 'react';

const squareSize = 50;
const shipNumber = 5;

export default class Room extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      input: '',
      canGameStart: false,
      youWon: false,
      gameFinished: false,
      postawioneStatki: 0,
      hitCount: 0,
      opponentHits: 0,
      isGameReady: false,
      isYourTurn: false,
      isPlayerReady: false,
      userBoard: [
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
      ],
      opponentBoard: [
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
      ],
    };

    this.onInput = this.onInput.bind(this);
    this.onSendMessage = this.onSendMessage.bind(this);
    this.registerGameReadyHandler = this.registerGameReadyHandler.bind(this);
    this.fireTorpedo = this.fireTorpedo.bind(this);
    this.setShip = this.setShip.bind(this);
    this.gameStatus = this.gameStatus.bind(this);
  }

  componentDidMount() {
    this.props.registerGameReadyHandler(this.registerGameReadyHandler);
    this.props.registerYourTurn((cords) => {
      if (cords) this.handleOpponentShot(cords);
      this.setState({ isYourTurn: true });
    });
    this.props.registerGameFinished(({ youWon }) => this.setState({ gameFinished: true, youWon }));
  }

  componentWillUnmount() {
    this.props.unregisterGameReadyHandler();
    this.props.unregisterYourTurn();
  }

  onInput(e) {
    this.setState({ input: e.target.value });
  }

  onSendMessage() {
    if (!this.state.input) return;

    this.props.onSendMessage(this.state.input, (err) => {
      if (err) return console.error(err);

      return this.setState({ input: '' });
    });
  }

  handleOpponentShot(cords) {
    const { userBoard } = this.state;
    const row = cords[0];
    const col = cords[1];
    const field = document.getElementById(`p${row}${col}`);
    if (userBoard[row][col] === 1) {
      field.style.background = 'red';
      this.setState((prevState) => ({ opponentHits: prevState.opponentHits + 1 }));
    } else {
      field.style.background = '#bbb';
    }
    console.log('cords:', cords);
  }

  registerGameReadyHandler(isReady) {
    console.log('registerGameReadyHandler triggered:', isReady);
    this.setState({ isGameReady: isReady });
  }

  setShip(e) {
    const { userBoard, postawioneStatki } = this.state;
    const tempBoard = userBoard;
    const row = e.target.id.substring(1, 2);
    const col = e.target.id.substring(2, 3);
    if (postawioneStatki < shipNumber) {
      if (userBoard[row][col] === 0) {
        e.target.style.background = 'blue';
        tempBoard[row][col] = 1;
        this.setState({ userBoard: tempBoard });
        this.setState(
          (prevState) => ({ postawioneStatki: prevState.postawioneStatki + 1, canGameStart: prevState.postawioneStatki + 1 === shipNumber }),
        );
      } else {
        alert('You set the ship here already!');
      }
    } else {
      alert('Postawiłeś juz wszystkie statki!');
    }
  }

  fireTorpedo(e) {
    const { opponentBoard, hitCount, isYourTurn } = this.state;

    const row = e.target.id.substring(1, 2);
    const col = e.target.id.substring(2, 3);
    if (isYourTurn) {
      this.setState({ isYourTurn: false });
      this.props.fireTorpedo(this.props.room.name, [parseInt(row, 10), parseInt(col, 10)], (result) => {
        console.log('fireTorpedo result', result);
        const field = document.getElementById(`o${row}${col}`);
        if (result === 0) {
          field.style.background = '#bbb';
          opponentBoard[row][col] = 3;
        } else if (result === 1) {
          field.style.background = 'red';
          opponentBoard[row][col] = 2;

          this.setState(
            (prevState) => ({ hitCount: prevState.hitCount + 1 }),
            () => {
              if (hitCount === shipNumber) {
                this.setState({ youWon: true });
              }
            },
          );
        } else if (opponentBoard[row][col] > 1) {
          alert('Stop wasting your torpedos! You already fired at this location.');
        }
      });
    } else {
      alert('To nie Twoja tura!');
    }
  }

  gameStatus() {
    const { isGameReady, gameFinished, isYourTurn } = this.state;
    if (isGameReady) {
      if (gameFinished) return 'Gra skończona';
      if (isYourTurn) return 'Twoja tura';
      return 'Tura przeciwnika';
    }

    return 'Oczekiwanie az wszyscy gracze ustawią swoje plansze';
  }

  render() {
    const { isGameReady, opponentBoard, canGameStart, userBoard, postawioneStatki, isPlayerReady, hitCount, opponentHits, gameFinished, youWon } = this.state;
    const { room, onLeave, setUserBoard } = this.props;
    return (
      <div className="h-100 relative flex flex-column items-center justify-start">
        <div className="w-100 flex justify-between items-center">
          <div />
          <h1 className="flex-auto">{`Pokój: ${room.name}`}</h1>
          <div className="br-pill bg-light-red white ph3 pv2" onClick={onLeave}>wyjdź</div>
        </div>
        <div className="flex w-100 flex-column justify-center items-center">
          <div className="" style={{ fontSize: 35 }}>{`${hitCount} : ${opponentHits}`}</div>
          <div className="flex w-100" style={{ fontSize: 12 }}>
            <p className="ma0 w-50 tr ph3 ttu">ty</p>
            <p className="ma0 ph3 ttu">przeciwnik</p>
          </div>
        </div>
        <div className="mt2 relative w-100 h-100 flex flex-column">
          {gameFinished
            ? (
              <div className="z-5 absolute top-0 left-0 w-100 bg-black-80 h-100 flex flex-auto justify-center items-center">
                <div className="br3 bg-black-90 pa3">
                  <p>{youWon ? 'Wygrałeś!' : 'Przeciwnik wygrał :('}</p>
                </div>
              </div>
            ) : null}
          <p className="tc">
            {this.gameStatus()}
          </p>
          <div className="flex mb3">
            <div className="w-50 flex flex-column justify-center items-center">
              <p>Twoja plansza:</p>
              <div className="gameboard relative" key="playerGameBoard" style={{ width: 300, height: 300 }}>
                {userBoard.map((row, i) => row.map((item, j) => (
                  <div
                    id={`p${i}${j}`}
                    style={{ top: `${i * squareSize}px`, left: `${j * squareSize}px` }}
                    onClick={this.setShip}
                    key={`p${i}${j}`}
                  >
                    {/* {item} */}
                  </div>
                )))}
              </div>
            </div>
            <div className="w-50 flex flex-column justify-center items-center">
              {isGameReady ? (
                <div>
                  <p>Plansza przeciwnika</p>
                  <div className="gameboard relative" key="opponentGameBoard" style={{ width: 300, height: 300 }}>
                    {opponentBoard.map((row, i) => row.map((item, j) => (
                      <div
                        id={`o${i}${j}`}
                        key={`o${i}${j}`}
                        style={{ top: `${i * squareSize}px`, left: `${j * squareSize}px` }}
                        onClick={this.fireTorpedo}
                      >
                        {/* {item} */}
                      </div>
                    )))}
                  </div>
                </div>
              ) : <div>{`Rozstawione statki na planszy: ${postawioneStatki}/${shipNumber}`}</div>}
            </div>
          </div>
          {isPlayerReady ? null : canGameStart ? (
            <div onClick={() => { this.setState({ isPlayerReady: true }); setUserBoard(room.name, userBoard); }} className="self-center bg-green pa3 br4 white">
              Ustaw gotowość
            </div>
          ) : <div className="self-center bg-light-gray pa3 br4 o-70">Ustaw gotowość</div>}
        </div>
      </div>
    );
  }
}

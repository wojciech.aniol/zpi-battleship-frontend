import React from 'react';
import GoogleLogin from 'react-google-login';
// import NewUser from './containers/NewUser';
import CreateRoom from './containers/CreateRoom';
import Ranking from './containers/Ranking';
import Room from './containers/Room';
import socket from './socket';
import ship from './assets/ship.png';
import bg from './assets/bg.svg';
import bgO from './assets/bg-o.png';

export default class Root extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      user: null,
      client: socket(),
      rooms: null,
      selectedRoom: null,
      roomHistory: null,
      isRankingShown: false,
      isAddingRoom: false,
    };

    this.onEnterRoom = this.onEnterRoom.bind(this);
    this.onLeaveRoom = this.onLeaveRoom.bind(this);
    this.getRooms = this.getRooms.bind(this);
    this.register = this.register.bind(this);
    this.setUserBoard = this.setUserBoard.bind(this);
    this.responseGoogle = this.responseGoogle.bind(this);
    this.onMessageReceived = this.onMessageReceived.bind(this);

    this.getRooms();
  }

  componentDidMount() {
    this.state.client.registerRoomsUpdate(this.getRooms);
    this.state.client.registerHandler(this.onMessageReceived);
  }

  componentWillUnmount() {
    this.state.client.unregisterRoomsUpdate();
    this.state.client.unregisterHandler();
  }

  onMessageReceived(entry) {
    this.updateRoomHistory(entry);
  }

  updateRoomHistory(entry) {
    console.log('new entry:', entry);
    this.setState((prevState) => ({
      roomHistory: prevState.roomHistory.concat(entry),
    }));
  }

  onEnterRoom(roomName, onNoUserSelected, onEnterSuccess) {
    if (!this.state.user) return onNoUserSelected();

    return this.state.client.join(roomName, (err, roomHistory) => {
      if (err) return console.error('error:', err);
      return onEnterSuccess(roomHistory);
    });
  }

  onLeaveRoom(roomName, onLeaveSuccess) {
    this.state.client.leave(roomName, (err) => {
      if (err) return console.error(err);
      return onLeaveSuccess();
    });
  }

  getRooms() {
    this.state.client.getRooms((err, rooms) => {
      this.setState({ rooms });
    });
  }

  register(name) {
    const onRegisterResponse = (user) => this.setState({ user });
    this.state.client.register(name, (err, user) => {
      if (err) return onRegisterResponse(null);
      return onRegisterResponse(user);
    });
  }

  newUser(name) {
    const onRegisterResponse = (user) => this.setState({ user });
    this.state.client.newUser(name, (err, user) => {
      if (err) return onRegisterResponse(null);
      return onRegisterResponse(user);
    });
  }

  createRoom(name) {
    this.setState({ isAddingRoom: false });
    this.state.client.createRoom(name, (err, user) => {
      console.log(err, user);
      this.forceUpdate();
    });
  }

  setUserBoard(room, board) {
    this.state.client.setUserBoard(room, board, () => console.log('setUserBoard tylko ze z frontu'));
  }

  responseGoogle(response) {
    console.log(response.profileObj);
    this.newUser(response.profileObj.email);
  }

  render() {
    const { rooms, selectedRoom, roomHistory, user, client, isAddingRoom } = this.state;

    return (
      <div className="h-100 w-100 bg-2 white flex flex-column">
        <div id="header" className="bg-2 pa3 flex justify-between items-center shadow-5 z-1">
          <p>{user || 'nieznany uzytkownik'}</p>
          <div className="flex items-center justify-center pa2">
            <div className="ma2 pointer" onClick={() => this.setState({ isRankingShown: true })}>RANKING</div>
          </div>
        </div>
        {user ? null : (
          <div className="fixed w-100 h-100 z-1" style={{ backgroundColor: 'rgb(40,40,40)' }}>
            <div className="flex justify-center items-center flex-column w-100 h-100">
              <h1 className="ma2 f2">Prawdziwa morska bitwa na Twoim komputerze</h1>
              <p className="w-50 tc mt3 f4 lh-title">Zbierz swoją flotę, przygotuj taktykę i walcz ze swoim przeciwnikiem! BitwaMorskaZPI jest całkowicie darmową grą dostępną dla kadego z dos†pem do internetu. A to wszystko w pięknej, minimalistycznej oprawie graficznej.</p>
              <div className="ma2">
                <GoogleLogin
                  clientId="648287464314-sdvd67fsusj4mh9d00lhi1jh6jcrdinf.apps.googleusercontent.com"
                  buttonText="Login"
                  onSuccess={this.responseGoogle}
                  onFailure={this.responseGoogle}
                  cookiePolicy="single_host_origin"
                  isSignedIn
                />

              </div>
            </div>
            <img src={ship} className="absolute bottom-1 z-1" alt="statek" style={{ right: '20%' }} />
            <img src={bg} className="absolute bottom-0 left-0 w-100" alt="statek-tlo" />
            <img src={bgO} className="absolute bottom-0 left-0 w-100 z-3 " alt="statek-tlo2" />
          </div>
        )}
        <div className="flex flex-auto" style={{ maxHeight: '100%' }}>
          <div className="w-20 bg-1 pa3 flex flex-column justify-between" style={{ minWidth: 200 }}>
            {rooms ? (
              <div>
                Dostępne pokoje:
                {rooms.map((room) => (
                  <div
                    key={room.name}
                    onClick={() => this.onEnterRoom(
                      room.name,
                      () => this.changeUserVisibility(true),
                      (_roomHistory) => this.setState({ roomHistory: _roomHistory, selectedRoom: room }),
                    )}
                  >
                    {`${room.name} - ${room.numMembers}/2`}
                  </div>
                ))}
              </div>
            ) : null}
            <div className="bg-white-30 br-pill ph4 pv3 center pointer f5" onClick={() => this.setState({ isAddingRoom: true })}>DODAJ NOWY POKOJ</div>
          </div>
          <div className="w-60 bg-2 pa2" style={{ minWidth: 700 }}>
            {user && selectedRoom && roomHistory ? (
              <Room
                room={selectedRoom}
                roomHistory={this.state.roomHistory}
                user={user}
                onLeave={() => this.onLeaveRoom(selectedRoom.name, () => this.setState({ selectedRoom: null, roomHistory: null }))}
                onSendMessage={(message, cb) => client.message(selectedRoom.name, message, cb)}
                registerGameReadyHandler={client.registerGameReadyHandler}
                unregisterGameReadyHandler={client.unregisterGameReadyHandler}
                setUserBoard={this.setUserBoard}
                fireTorpedo={client.fireTorpedo}
                registerYourTurn={client.registerYourTurn}
                unregisterYourTurn={client.unregisterYourTurn}
                registerGameFinished={client.registerGameFinished}
                unregisterGameFinished={client.unregisterGameFinished}
              />
            ) : <div className="flex w-100 h-100 justify-center items-center"><p cloassName="">Wybierz pokoj z listy obok, aby rozpocząć</p></div>}
          </div>
          <div className="w-20 bg-1 pa3" style={{ minWidth: 200, maxHeight: '100%' }}>
            <div className="overflow-y-auto" style={{ fontSize: 12, maxHeight: '100%' }}>
              <p style={{ fontSize: 16 }}>Aktywność</p>
              {roomHistory
              // eslint-disable-next-line no-shadow
                ? this.state.roomHistory.map(({ user, message, event }, i) => {
                  console.log(user, message, event);
                  return (
                    <div key={i}>
                      <p>{`${user ? user.split('@')[0] : ''} ${event || ''}`}</p>
                    </div>
                  );
                })
                : null}
            </div>
          </div>
        </div>
        {this.state.isRankingShown ? <Ranking closeRanking={() => this.setState({ isRankingShown: false })} /> : null}
        {isAddingRoom ? (
          <div className="fixed top-0 left-0 w-100 h-100 bg-black-60 flex justify-center items-center">
            <CreateRoom
              createRoom={(name) => this.createRoom(name)}
              getRooms={() => this.getRooms()}
            />
          </div>
        ) : null}
      </div>
    );
  }
}
